using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float jumpAmount;

    public Animator animator;
    public ContactFilter2D contactFilter;
    public GameManager gameManager;
    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        animator.SetFloat("Speed", rb.velocity.magnitude);

        Vector2 move = new Vector2();
        move.x = Input.GetAxis("Horizontal") * speed;

        if (Input.GetButtonDown("Jump")) {
            if (rb.IsTouching(contactFilter)) {
                move.y = jumpAmount;
            }
        }

        rb.AddForce(move);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Coin")) {
            gameManager.IncrementCoinCount();
            Destroy(other.gameObject);
        }
    }
}
