using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    int currentCoins = 0;
    public TextMeshProUGUI textMesh;

    public void IncrementCoinCount() {
        currentCoins += 1;
        textMesh.text = "Coins: " + currentCoins.ToString();
    }

    public void EndGame() {
        StartCoroutine(EndGameCoroutine());
    }

    IEnumerator EndGameCoroutine() {
        textMesh.text = "You win! Total Collected Coins: " + currentCoins.ToString();
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene(0);
        yield break;
    }
}
