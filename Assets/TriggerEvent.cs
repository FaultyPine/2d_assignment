using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour
{
    public GameManager gameManager;
    void Start()
    {
        
    }

    void OnTriggerEnter2D( Collider2D other ) {
        Destroy(this.gameObject);
        gameManager.EndGame();
    }

    void Update()
    {
        
    }
}
